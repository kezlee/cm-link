// GENERAL VARIABLE
var mobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? true : false;
console.log('mobile', mobile);
$('.info').on('click', function(e){
  e.preventDefault();

  if ( $(this).hasClass('open') ) {
    $(this).removeClass('open');
    $('.find-number').slideUp();
  } else {
    $(this).addClass('open');
    $('.find-number').slideDown();
  }
});

// check if other amount is checked, show input
$('input[name=topupamount]').on('change', function(){
  if ( $('input[name=topupamount]:checked').val() === 'other amount' ) {
    $(this).parent().parent().addClass('enter-amount-show');
    $('input[name=otheramount]').fadeIn().focus();
    $('input[name=otheramount]').attr('required', true);


    $('.button-wrapper a').on('click', function(e){
      e.preventDefault();

      var currency = $(this).text().substring(0, 3);

      if ( currency == 'GBP' ) {
        $(this).parent().prev().prev().text('£');
      } else {
        $(this).parent().prev().prev().text('¥');
      }

      if ( !$(this).hasClass('active') ) {
        $('.button-wrapper a').removeClass('active');
        $(this).addClass('active');
      }

    });

  } else {
    $('.radio').removeClass('enter-amount-show');
    $('input[name=otheramount]').fadeOut();
    $('input[name=otheramount]').attr('required', false);
  }
});
// append error msg when invalid
$('form[name=topup-amount]').on('invalid.bs.validator', function(e){
  $('.wrapper-error').hide();
  $('.wrapper-error p').text(e.detail[0]);
});

$('form[name=topup-amount]').validator().on('submit', function (e) {
  if (!e.isDefaultPrevented()) {
    showLoader();
  } else {
    $('.wrapper-error').show();
  }
});

var showLoader = function() {
  $('.container-loading').show();
};
var hideLoader = function() {
  $('.container-loading').hide();
};

// SLIDER DATA
var startVal;
var stopVal;

var sliderVertical = $('#slider-vertical').slider();
var sliderHorizontal = $('#slider-horizontal').slider();

var resetAllLabel = function() {
  $('.slider-horizontal .slider-tick-label-container .slider-tick-label').fadeOut();
};
var show9gbLabel = function() {
  $($('.slider-horizontal .slider-tick-label-container .slider-tick-label')[0]).fadeIn();
};
var show18gbLabel = function() {
  $($('.slider-horizontal .slider-tick-label-container .slider-tick-label')[1]).fadeIn();
};
var show27gbLabel = function() {
  $($('.slider-horizontal .slider-tick-label-container .slider-tick-label')[2]).fadeIn();
};

sliderVertical
.on('slideStart', function(slider) {
  startVal = slider.value;
}).on('slideStop', function(slider){
  stopVal = slider.value;
  onChangeSlider(startVal, stopVal);
});

sliderHorizontal
.on('slideStart', function(slider) {
  startVal = slider.value;
}).on('slideStop', function(slider){
  stopVal = slider.value;
  onChangeSlider(startVal, stopVal);
});

function onChangeSlider(start, stop) {

  var img27Gb = (mobile) ? $('.slide-img.mobile.one') : $('.slide-img.desktop.three') ;
  var img18Gb =  $('.slide-img.two');

  if ( stop > start ) {
    if ( stop > 20 && stop <= 30 ) {

      if (mobile) {
        // console.log('show 27 mobile');
        resetAllLabel();
        show27gbLabel();
        if (img18Gb.hasClass('fadeOutDown')) {
          img18Gb.removeClass('fadeOutDown');
        }
        img18Gb.addClass('fadeInUp');
        if (img27Gb.hasClass('fadeOutDown')) {
          img27Gb.removeClass('fadeOutDown');
        }
        img27Gb.addClass('fadeInUp');

      } else {
        // console.log('hide 18 desktop');
        if (img27Gb.hasClass('fadeInUp')) {
          img27Gb.removeClass('fadeInUp');
        }
        img27Gb.addClass('fadeOutDown');
        if (img18Gb.hasClass('fadeInUp')) {
          img18Gb.removeClass('fadeInUp');
        }
        img18Gb.addClass('fadeOutDown');

      }


      sliderHorizontal.slider('setValue', 30);
      sliderVertical.slider('setValue', 30);

    } else if ( stop > 10 && stop <=20 ) {


      if (mobile) {
        // console.log('show 18 mobile');
        resetAllLabel();
        show18gbLabel();
        if (img18Gb.hasClass('fadeOutDown')) {
          img18Gb.removeClass('fadeOutDown');
        }

        img18Gb.addClass('fadeInUp');

      } else {
        // console.log('hide 27 desktop');
        if (img27Gb.hasClass('fadeInUp')) {
          img27Gb.removeClass('fadeInUp');
        }
        img27Gb.addClass('fadeOutDown');
      }

      sliderHorizontal.slider('setValue', 20);
      sliderVertical.slider('setValue', 20);
    }

  } else if ( stop < start ) {

    if ( stop >= 20 && stop < 30 ) {


      if (mobile) {
        resetAllLabel();
        show18gbLabel();
        // console.log('hide 27 mobile');
        if (img27Gb.hasClass('fadeInUp')) {
          img27Gb.removeClass('fadeInUp');
        }
        img27Gb.addClass('fadeOutDown');

      } else {

        // console.log('show 18 desktop');
        if (img18Gb.hasClass('fadeOutDown')) {
          img18Gb.removeClass('fadeOutDown');
        }
        img18Gb.addClass('fadeInUp');

      }


      sliderHorizontal.slider('setValue', 20);
      sliderVertical.slider('setValue', 20);

    } else if ( stop >= 10 && stop < 20 ) {

      if (mobile) {

        // console.log('hide 18 mobile');
        resetAllLabel();
        show9gbLabel();
        if (img18Gb.hasClass('fadeInUp')) {
          img18Gb.removeClass('fadeInUp');
        }
        img18Gb.addClass('fadeOutDown');

        if (img27Gb.hasClass('fadeInUp')) {
          img27Gb.removeClass('fadeInUp');
        }
        img27Gb.addClass('fadeOutDown');

      } else {
        // console.log('show 27 desktop');
        if (img27Gb.hasClass('fadeOutDown')) {
          img27Gb.removeClass('fadeOutDown');
        }

        if (img18Gb.hasClass('fadeOutDown')) {
          img18Gb.removeClass('fadeOutDown');
        }

        img18Gb.addClass('fadeInUp');
        img27Gb.addClass('fadeInUp');

      }

      sliderHorizontal.slider('setValue', 10);
      sliderVertical.slider('setValue', 10);

    }
  }
}


// DRAW DOT ON END OF CIRCULAR PROGRESS
(function() {

  var proto = $.circleProgress.defaults,
      superInitWidget = proto.initWidget,
      superDrawFrame = proto.drawFrame;

  $.extend(proto, {
    dotRadius: 9,

    initWidget: function() {
      superInitWidget.call(this);
      this.dotOffset = this.dotRadius - this.getThickness() / 2;
      this.radius -= this.dotOffset;
    },

    drawFrame: function(v) {
      this.ctx.save();
      this.ctx.clearRect(0, 0, this.size, this.size);
      this.ctx.translate(this.dotOffset, this.dotOffset);
      superDrawFrame.call(this, v);
      // this.drawDot();
      this.drawDot(v);
      this.ctx.restore();
    },

    drawDot: function(v) {
      var ctx = this.ctx,
          sa = this.startAngle + 2 * Math.PI * (v || 0),
          r = this.radius,
          rd = r - this.getThickness() / 2,
          x = r + rd * Math.cos(sa),
          y = r + rd * Math.sin(sa);

      ctx.save();
      // ctx.fillStyle = this.arcFill;
      ctx.fillStyle = '#000';
      ctx.beginPath();
      ctx.arc(x, y, this.dotRadius, 0, 2 * Math.PI);
      ctx.fill();
      ctx.restore();
    }
  });

})();

// When scroll to the CIRCULAR PROGRESS
var waypoint = new Waypoint({
  element: document.getElementById('packages'),
  handler: function(direction) {
    if ( direction === 'down' ) {
      circle1();
      circle2();
      circle3();
    }
  },
  offset: '25%'
});

$('.circle-bar.one').on('mouseover', function(){
  circle1();
});
$('.circle-bar.two').on('mouseover', function(){
  circle2();
});
$('.circle-bar.three').on('mouseover', function(){
  circle3();
});

var circle1 = function() {
  $('.circle-bar.one').circleProgress({
    value: 0.75,
    size: 140,
    startAngle: -Math.PI / 4 * 2,
    thickness: 15,
    lineCap: 'round',
    fill: {
      gradient: [['#f7931e', .5], ['#f7cfa0', .5]], gradientAngle: -Math.PI / 4 * 3
      // gradient: ["red", "orange"]
    }
  });
};
var circle2 = function() {
  $('.circle-bar.two').circleProgress({
    value: 0.70,
    size: 160,
    startAngle: -Math.PI / 4 * 2,
    thickness: 15,
    lineCap: 'round',
    fill: {
      gradient: [['#a5dcbe', .5], ['#29b468', .5]], gradientAngle: 1 / 10
      // gradient: ["red", "orange"]
    }
  });
};
var circle3 = function() {
  $('.circle-bar.three').circleProgress({
    value: 0.70,
    size: 180,
    startAngle: -Math.PI / 4 * 2,
    thickness: 15,
    lineCap: 'round',
    fill: {
      gradient: [['#94d0ed', .5], ['#0096df', .5]], gradientAngle: 1 / 10
      // gradient: ["red", "orange"]
    }
  });
};

if( mobile ) {
  $('.package-section.mobile').owlCarousel({
    loop:true,
    nav:false,
    margin:10,
    items:1,
    stagePadding: 30
  });

  $('.owl-carousel').owlCarousel({
    loop:true,
    nav:true,
    margin:10,
    items:1,
    navText: ''
  });
}

$('.btn-view-details').on('click', function(e){
  e.preventDefault();
  if ( $(this).hasClass('active') ) {
    $(this).removeClass('active');
    if ( mobile ) {
      $(this).parent().parent().parent().next().slideUp();
    } else {
      $(this).parent().next().slideUp();
    }

  } else {
    $(this).addClass('active');
    if( mobile ) {
      $(this).parent().parent().parent().next().slideDown();
    } else {
      $(this).parent().next().slideDown();
    }

  }

});
